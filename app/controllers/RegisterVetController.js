app.controller('RegisterVetController',
    function(eventService, $mdDialog, $routeParams) {
        var self = this;

        self.newVet = {};
        self.newVet.visitors = 0;
        self.registerVet = function() {

            var vetToRegister = self.newVet;
            vetToRegister.profileImg = randomImage();

            eventService.registerVet($routeParams.index,vetToRegister);
            $mdDialog.hide(true);
        };

        self.cancel = function() {
            $mdDialog.cancel();
        };

        var images = [
            'resources/images/100-0.jpeg',
            'resources/images/100-1.jpeg',
            'resources/images/100-2.jpeg',
            'resources/images/100-3.jpeg',
            'resources/images/100-4.jpeg',
            'resources/images/100-5.jpeg',
            'resources/images/100-6.jpeg',
            'resources/images/100-7.jpeg',
            'resources/images/100-8.jpeg',
            'resources/images/100-9.jpeg'];

        function randomImage() {
            var randomNum = Math.floor(Math.random() * images.length);
            return images[randomNum];
        }
    });
