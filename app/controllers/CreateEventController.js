app.controller('CreateEventController', 
function(eventService, $mdDialog) {
    var self = this;

    self.newEvent = {};
    self.chapterState = '';
    self.chapters = ['Alabama', 'California', 'Delaware', 'Florida', 'Hawaii', 'Idaho', 'Kentucky', 'Maryland', 'Montana', 'Nevada']
        .map(function(chapter) {
            return {
                title: chapter
            };
        });

    self.eventState = '';
    self.eventType = ['Kayak', 'Non-Kayak', 'Fishing']
        .map(function(type) {
            return {
                title: type
            };
        });
    self.newEvent.days = 1;

    self.eventDate = new Date();
    self.minDate = new Date(
        self.eventDate.getFullYear(),
        self.eventDate.getMonth() - 2,
        self.eventDate.getDate());
        self.newEvent.date = self.eventDate;

    self.newEvent.fromTime = self.eventDate.getUTCHours() + ':' + self.eventDate.getUTCMinutes();
    self.newEvent.toTime = (self.eventDate.getUTCHours() +1) + ':' + self.eventDate.getUTCMinutes();
    
    self.addEvent = function() {
    	console.log('TODO: Register a new Event');

    	var eventToAdd = self.newEvent;
    	eventToAdd.chapter = self.chapterState;
    	eventToAdd.type = self.eventState;
        eventToAdd.vets = [];
        eventToAdd.volunteers = [];

    	console.log('eventToAdd:', eventToAdd);

    	eventService.addEvent(eventToAdd);
    	$mdDialog.hide(true);
    };

    self.cancel = function() {
    	$mdDialog.cancel();
    };

    
});