app.controller('LoginController', 
function LoginController(eventService, $mdSidenav, $mdBottomSheet,
    $timeout, $log, $scope, $mdDialog, $location) {
    var self = this;

    self.username = '';
    self.password = '';

    self.login = function() {
        // Callback function which switches page
        $location.path('#/events');
    };
});