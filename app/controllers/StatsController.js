app.controller('StatsController', function(eventService, $mdDialog) {
    var self = this;
    self.eventObj = undefined;
    self.augustTotals = {};
    self.septemberTotals = {};
    self.octoberTotals = {};
    self.novemberTotals = {};
    self.decemberTotals = {};
    eventService.getEvents().then(function(evt) {
        self.eventObj = evt;
        // console.log('Event: ', self.eventObj);

        // return events from specified month
        var augEvents = getEventsByMonth(self.eventObj, 7);
        self.augustTotals.totalEvents = getTotalEvents(augEvents);
        self.augustTotals.totalMaleVets = getGenderTotals(augEvents, 'male');
        self.augustTotals.totalFemaleVets = getGenderTotals(augEvents, 'female');
        self.augustTotals.totalFirstTimeVets = getTotalFirstTimeVets(augEvents);
        self.augustTotals.totalVisitors = getTotalVisitors(augEvents);
        self.augustTotals.totalFirstTimeVisitors = getTotalFirstTimeVisitors(augEvents);
        self.augustTotals.totalEventVolunteers = getTotalEventVolunteers(augEvents);
        self.augustTotals.totalFirstTimeVolunteers = getTotalFirstTimeVolunteers(augEvents);
        self.augustTotals.totalWarriorVolunteers = getTotalWarriorVolunteers(augEvents);
        self.augustTotals.totalVolunteerHours = getTotalVolunteerHours(augEvents);
        self.augustTotals.totalOutingDays = getTotalOutingDays(augEvents);

        var septEvents = getEventsByMonth(self.eventObj, 8);
        self.septemberTotals.totalEvents = getTotalEvents(septEvents);
        self.septemberTotals.totalMaleVets = getGenderTotals(septEvents, 'male');
        self.septemberTotals.totalFemaleVets = getGenderTotals(septEvents, 'female');
        self.septemberTotals.totalFirstTimeVets = getTotalFirstTimeVets(septEvents);
        self.septemberTotals.totalVisitors = getTotalVisitors(septEvents);
        self.septemberTotals.totalFirstTimeVisitors = getTotalFirstTimeVisitors(septEvents);
        self.septemberTotals.totalEventVolunteers = getTotalEventVolunteers(septEvents);
        self.septemberTotals.totalFirstTimeVolunteers = getTotalFirstTimeVolunteers(septEvents);
        self.septemberTotals.totalWarriorVolunteers = getTotalWarriorVolunteers(septEvents);
        self.septemberTotals.totalVolunteerHours = getTotalVolunteerHours(septEvents);
        self.septemberTotals.totalOutingDays = getTotalOutingDays(septEvents);

        var octEvents = getEventsByMonth(self.eventObj, 9);
        self.octoberTotals.totalEvents = getTotalEvents(octEvents);
        self.octoberTotals.totalMaleVets = getGenderTotals(octEvents, 'male');
        self.octoberTotals.totalFemaleVets = getGenderTotals(octEvents, 'female');
        self.octoberTotals.totalFirstTimeVets = getTotalFirstTimeVets(octEvents);
        self.octoberTotals.totalVisitors = getTotalVisitors(octEvents);
        self.octoberTotals.totalFirstTimeVisitors = getTotalFirstTimeVisitors(octEvents);
        self.octoberTotals.totalEventVolunteers = getTotalEventVolunteers(octEvents);
        self.octoberTotals.totalFirstTimeVolunteers = getTotalFirstTimeVolunteers(octEvents);
        self.octoberTotals.totalWarriorVolunteers = getTotalWarriorVolunteers(octEvents);
        self.octoberTotals.totalVolunteerHours = getTotalVolunteerHours(octEvents);
        self.octoberTotals.totalOutingDays = getTotalOutingDays(octEvents);

        var novEvents = getEventsByMonth(self.eventObj, 10);
        self.novemberTotals.totalEvents = getTotalEvents(novEvents);
        self.novemberTotals.totalMaleVets = getGenderTotals(novEvents, 'male');
        self.novemberTotals.totalFemaleVets = getGenderTotals(novEvents, 'female');
        self.novemberTotals.totalFirstTimeVets = getTotalFirstTimeVets(novEvents);
        self.novemberTotals.totalVisitors = getTotalVisitors(novEvents);
        self.novemberTotals.totalFirstTimeVisitors = getTotalFirstTimeVisitors(novEvents);
        self.novemberTotals.totalEventVolunteers = getTotalEventVolunteers(novEvents);
        self.novemberTotals.totalFirstTimeVolunteers = getTotalFirstTimeVolunteers(novEvents);
        self.novemberTotals.totalWarriorVolunteers = getTotalWarriorVolunteers(novEvents);
        self.novemberTotals.totalVolunteerHours = getTotalVolunteerHours(novEvents);
        self.novemberTotals.totalOutingDays = getTotalOutingDays(novEvents);

        var decEvents = getEventsByMonth(self.eventObj, 11);
        self.decemberTotals.totalEvents = getTotalEvents(decEvents);
        self.decemberTotals.totalMaleVets = getGenderTotals(decEvents, 'male');
        self.decemberTotals.totalFemaleVets = getGenderTotals(decEvents, 'female');
        self.decemberTotals.totalFirstTimeVets = getTotalFirstTimeVets(decEvents);
        self.decemberTotals.totalVisitors = getTotalVisitors(decEvents);
        self.decemberTotals.totalFirstTimeVisitors = getTotalFirstTimeVisitors(decEvents);
        self.decemberTotals.totalEventVolunteers = getTotalEventVolunteers(decEvents);
        self.decemberTotals.totalFirstTimeVolunteers = getTotalFirstTimeVolunteers(decEvents);
        self.decemberTotals.totalWarriorVolunteers = getTotalWarriorVolunteers(decEvents);
        self.decemberTotals.totalVolunteerHours = getTotalVolunteerHours(decEvents);
        self.decemberTotals.totalOutingDays = getTotalOutingDays(decEvents);
    });

    function getTotalEvents(events){
        return events.length;
    }

    function getTotalOutingDays(events){
        var totalOutingDays = 0;
        events.forEach(function(item){
            totalOutingDays+=parseInt(item.days);
        })
        return totalOutingDays;
    }


    function getGenderTotals(events, gender){
        var genderTotal = 0;
        events.forEach(function(item){
            for(var i=0;i<item.vets.length;i++){
                if(item.vets[i].gender === gender){
                    genderTotal++;
                }
            }
        });
        return genderTotal;
    }

    function getTotalVisitors(events){
        var visitors = 0;
        events.forEach(function(item){
            for(var i=0;i<item.vets.length;i++){
                console.log(parseInt(item.vets[i].visitors))
                visitors+=parseInt(item.vets[i].visitors);
            }
        });
        return visitors;
    }

    function getTotalFirstTimeVisitors(events){
        var firstTimeVisitors = 0;
        events.forEach(function(item){
            for(var i=0;i<item.vets.length;i++){
                if(item.vets[i].firstTimeVisitors === true){
                    firstTimeVisitors+= parseInt(item.vets[i].visitors);
                }
            }
        });
        return firstTimeVisitors;
    }

    function getTotalFirstTimeVets(events){
        var firstTimeVets = 0;
        events.forEach(function(item){
            for(var i=0;i<item.vets.length;i++){
                if(item.vets[i].firstTimeVet === true){
                    firstTimeVets++;
                }
            }
        });
        return firstTimeVets;
    }

    function getTotalEventVolunteers(events){
        var volunteers = 0;
        events.forEach(function(item){
            for(var i=0;i<item.volunteers.length;i++){
                volunteers++;
            }
        });
        return volunteers;
    }

    function getTotalFirstTimeVolunteers(events){
        var firstTimeVolunteers = 0;
        events.forEach(function(item){
            for(var i=0;i<item.volunteers.length;i++){
                if(item.volunteers[i].firstTimeVolunteer === true){
                    firstTimeVolunteers++;
                }
            }
        });
        return firstTimeVolunteers;
    }

    function getTotalWarriorVolunteers(events){
        var warriorVolunteers = 0;
        events.forEach(function(item){
            for(var i=0;i<item.volunteers.length;i++){
                if(item.volunteers[i].warriorVolunteer === true){
                    warriorVolunteers++;
                }
            }
        });
        return warriorVolunteers;
    }

    function getTotalVolunteerHours(events){
        var volunteerHours = 0;
        events.forEach(function(item){
            for(var i=0;i<item.volunteers.length;i++){
                volunteerHours+=parseInt(item.volunteers[i].volunteerHours);
            }
        });
        return volunteerHours;
    }

    function getEventsByMonth(events, month){
        var event = events.filter(function(item){
            return item.date.getUTCMonth() === month;
        })
        return event;
    }
});