app.controller('EventController', function(eventService, $mdSidenav, $mdBottomSheet, $timeout, $log, $scope, $mdDialog, $location) {
    var self = this;

    self.events = [];
    /*self.selectEvent = selectEvent*/
    self.makeContact = makeContact;

    // Load all events
    eventService.getEvents().then(function(events) {
        self.events = events;
    });

    self.viewEvent = function(event) {
        var newView = '#/eventDetails/' + self.events.indexOf(event);
        console.log('Navigating to ' + newView);
        $location.path(newView);
    };

    /*self.addEvent = function() {
        var event = {
            chapter: 'New Chapter',
            date: '00-00-00',
            type: 'new Type',
            outing_days: 10,
            vets: [],
            volunteers: []
        };
        self.events.push(event);
        console.log(self.events);
        // console.log('Event Added');
    };

    self.login = function() {
        location.href = '#/login';
    };
    self.events = function() {
        location.href = '#/events';
    }
    self.home = function() {
        location.href = '#/home';
    };*/




    
    // *********************************
    // Internal methods
    // *********************************

    /**
     * Show the Contact view in the bottom sheet
     */
    function makeContact(selectedEvent) {

        $mdBottomSheet.show({
            controllerAs: "vm",
            templateUrl: 'views/contactSheet.html',
            controller: ['$mdBottomSheet', ContactSheetController],
            parent: angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
            $log.debug(clickedItem.name + ' clicked!');
        });


    }

});

