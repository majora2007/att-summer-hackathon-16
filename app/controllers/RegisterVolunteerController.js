app.controller('RegisterVolunteerController',
    function(eventService, $mdDialog, $routeParams) {
        var self = this;

        self.newVolunteer = {};
        self.registerVolunteer = function() {

            var volunteerToRegister = self.newVolunteer;
            volunteerToRegister.profileImg = randomImage();

            eventService.registerVolunteer($routeParams.index,volunteerToRegister);
            $mdDialog.hide(true);
        };

        self.cancel = function() {
            $mdDialog.cancel();
        };

        var images = [
            'resources/images/100-0.jpeg',
            'resources/images/100-1.jpeg',
            'resources/images/100-2.jpeg',
            'resources/images/100-3.jpeg',
            'resources/images/100-4.jpeg',
            'resources/images/100-5.jpeg',
            'resources/images/100-6.jpeg',
            'resources/images/100-7.jpeg',
            'resources/images/100-8.jpeg',
            'resources/images/100-9.jpeg'];

        function randomImage() {
            var randomNum = Math.floor(Math.random() * images.length);
            return images[randomNum];
        }
    });
