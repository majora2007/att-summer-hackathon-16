app.controller('EventDetailsController', function($location, $routeParams, eventService, $mdDialog) {
	var self = this;
	self.eventObj = undefined;
	eventService.getEvent($routeParams.index).then(function(evt) {
		self.eventObj = evt;
		console.log('Event: ', self.eventObj);
	});

	self.launchRegisterVetModal = function(evt){
		$mdDialog.show({
			parent: angular.element(document.body),
			clickOutsideToClose: false,
			controller: 'RegisterVetController',
			templateUrl:'views/registerVet.html',
			targetEvent: evt,
			bindToController: true,
			locals: {event: self.events}
		}).then(function(newVet){
			console.log('Vet:',newVet);
		}, function(){
			console.log('You cancelled the dialog.');
		});
	};

	self.launchRegisterVolunteerModal = function(evt){
		$mdDialog.show({
			parent: angular.element(document.body),
			clickOutsideToClose: false,
			controller: 'RegisterVolunteerController',
			templateUrl:'views/registerVolunteer.html',
			targetEvent: evt,
			bindToController: true,
			locals: {event: self.events}
		}).then(function(newVolunteer){
			console.log('Vet:',newVolunteer);
		}, function(){
			console.log('You cancelled the dialog.');
		});
	};
});