var app = angular.module('howApp', ['ngMaterial', 'ngMdIcons', 'ngRoute']);
app.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/loginView.html'
            })
            .when('/events', {
                templateUrl: 'views/eventsView.html'
            })
            .when('/login', {
                templateUrl: 'views/loginView.html'
            })
            .when('/eventDetails/:index', {
                templateUrl: 'views/eventDetails.html'
            })
            .when('/stats', {
                templateUrl: 'views/stats.html'
            })
            .when('/profile', {
                templateUrl: 'views/profileView.html'
            })
            .when('/graphs', {
                templateUrl: 'views/graphs.html'
            })
            .otherwise({
                redirectTo: '/events'
            });
    });
app.config(function($mdThemingProvider, $mdIconProvider) {

    $mdIconProvider
        .defaultIconSet("./assets/svg/avatars.svg", 128)
        .icon("menu", "./assets/svg/menu.svg", 24)
        .icon("share", "./assets/svg/share.svg", 24)
        .icon("google_plus", "./assets/svg/google_plus.svg", 512)
        .icon("hangouts", "./assets/svg/hangouts.svg", 512)
        .icon("twitter", "./assets/svg/twitter.svg", 512)
        .icon("phone", "./assets/svg/phone.svg", 512);

    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('grey');

});
/*
app.run(['$rootScope', '$location', 'Auth', function($rootScope, $location, Auth) {
    $rootScope.$on('$routeChangeStart', function(event, currRoute, prevRoute){
        var logged = Auth.isLogin();

        //check if the user is going to the login page
        // i use ui.route so not exactly sure about this one but you get the picture
        var appTo = currRoute.path.indexOf('/secure') !== -1;

        if(appTo && !logged) {
            event.preventDefault();
            $location.path('/login');
        }
    });
}]);
*/

app.controller('ParentController', function($mdDialog, $location) {
    /* This is the parent controller and holds global info, like profile */
    var self = this;
    self.profile = {'authenticated': true, 'admin': true};


    self.login = function(username, password, callback) {
        if (username !== '') {
            self.profile.username = username;
            self.profile.authenticated = true;
        }

        if (username === 'joe') {
            self.profile.admin = true;
        }

        if (angular.isFunction(callback)) {
            callback();
        }
    };

    self.navigate = function(url) {
        console.log('Navigating to ' + url);
        $location.path(url);
    };

    self.launchCreateEventModal = function(evt){
        $mdDialog.show({
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            controller: 'CreateEventController',
            templateUrl:'views/createEvent.html',
            targetEvent: evt,
            bindToController: true,
            locals: {event: self.events}
        }).then(function(newEvent){
           angular.noop();
        }, function(){
            angular.noop();
        });
    };
});
