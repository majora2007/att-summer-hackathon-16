app.factory('eventService', function($q) {
  var events = [
      {
        title: 'Kayaking in Arizona',
        chapter: 'Arizona',
        date: new Date('2016-10-10'),
        type: 'kayak',
        fromTime: '10:00',
        toTime: '15:00',
        days: 1,
        vets: [
			{
			    firstName: 'James',
                lastName: 'Bond',
                gender: 'male',
                firstTimeVet: true,
                visitors: 3,
                firstTimeVisitors: true,
				profileImg : 'resources/images/100-0.jpeg'
			},
			{
                firstName: 'Jackie',
                lastName: 'Chan',
                gender: 'male',
                firstTimeVet: false,
                visitors: 1,
                firstTimeVisitors: false,
				profileImg : 'resources/images/100-1.jpeg'
			}
		],
        volunteers: [
			{
			    firstName: 'Jimmy',
                lastName: 'Fallon',
                firstTimeVolunteer: true,
                warriorVolunteer: true,
                volunteerHours: 5,
				profileImg : 'resources/images/100-2.jpeg'
			},
			{
                firstName: 'Katie',
                lastName: 'Couric',
                firstTimeVolunteer: false,
                warriorVolunteer: true,
                volunteerHours: 2,
				profileImg : 'resources/images/100-1.jpeg'
			}
		],
        notes: ''
      }
  ];

  var _addEvent = function(event) {
    events.push(event);
    return $q.when(events)
  };
  var _getEvents = function() {
    return $q.when(events);
  };

  var _getEvent = function(index) {
    return $q.when(events[index]);
  };

  var _registerVet = function(index, vet){
    return $q.when(events[index].vets.push(vet));
  };

    var _registerVolunteer = function(index, volunteer){
        return $q.when(events[index].volunteers.push(volunteer));
    };
  return {
    getEvents: _getEvents,
    getEvent: _getEvent,
    addEvent: _addEvent,
    registerVet: _registerVet,
    registerVolunteer: _registerVolunteer
  };
});

  /*function EventService($q){
    var events = [
      {
        chapter: 'Arizona',
        date: '2016-10-10',
        type: 'kayak',
        outing_days:1,
        vets:[],
        volunteers:[]
      },
      // {
      //   chapter: 'Texas',
      //   date: '2016-11-23',
      //   type: 'kayak',
      //   outing_days:2,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'Oklahoma',
      //   date: '2016-09-01',
      //   type: 'non-kayak',
      //   outing_days:1,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'California',
      //   date: '2016-12-15',
      //   type: 'kayak',
      //   outing_days:3,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'New Mexico',
      //   date: '2016-09-10',
      //   type: 'kayak',
      //   outing_days:1,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'Arizona',
      //   date: '2016-10-20',
      //   type: 'non-kayak',
      //   outing_days:1,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'California',
      //   date: '2016-11-25',
      //   type: 'kayak',
      //   outing_days:1,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'Texas',
      //   date: '2016-12-05',
      //   type: 'kayak',
      //   outing_days:1,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'Florida',
      //   date: '2016-09-21',
      //   type: 'kayak',
      //   outing_days:2,
      //   vets:[],
      //   volunteers:[]
      // },
      // {
      //   chapter: 'Florida',
      //   date: '2016-11-18',
      //   type: 'kayak',
      //   outing_days:1,
      //   vets:[],
      //   volunteers:[]
      // },

    ];

    // Promise-based API
    return {
      loadAllEvents : function() {
        // Simulate async nature of real remote calls
        return $q.when(events);
      }
      // addEvent : function(){
      //   var event = {
      //     chapter: 'New Chapter',
      //     date: '00-00-00',
      //     type: 'new Type',
      //     outing_days: 10,
      //     vets:[],
      //     volunteers:[]
      //   };
      //   events.push(event);
      //   return $q.when(events);
      //
      // }

    };
  }*/
